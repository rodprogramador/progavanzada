package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.Conexion;

/**
 * Servlet implementation class Devolucion
 */
@WebServlet("/Devolucion")
public class Devolucion extends HttpServlet {
	   Connection con;
	   Statement st;
	   ResultSet rs;       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Devolucion() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
		try{
			 
			 String codlib    = request.getParameter("txtid");
	  	   	 String query        = "";
             con = Conexion.getConnection();
             st=con.createStatement();
             query = "UPDATE libro SET estado= ' 1 ' , reservado = 'no' where libro.id='"+codlib+"'";
             st.executeUpdate(query);
             response.sendRedirect("HomeCliente.jsp" );
  	    	 con.close();
          }
		  catch(Exception e){
			  	  e.printStackTrace();
		  }
	}

}
