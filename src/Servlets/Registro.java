package Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;

import clases.Conexion;

/**
 * Servlet implementation class Registro
 */
@WebServlet("/Registro")
public class Registro extends HttpServlet {
	   Connection con;
	   Statement st;

	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		response.setContentType("text/html;charset=UTF-8");
		
	      try{
	  	       String nombre    = request.getParameter("txtNombre");
	           String apellido  = request.getParameter("txtApellido");
	           String password  = request.getParameter("txtPass");
	           String direccion = request.getParameter("txtDire");
	           String telefono  = request.getParameter("txtTel");
	           String query     = "";
	           con=Conexion.getConnection();
	           st=con.createStatement();
	           query = "INSERT INTO usuario( " + "nombre,  " + "apellido, " + "telefono, " + "direccion, "
	                    + "pass" + ") VALUES (?,?,?,?,?)";
	           PreparedStatement st = null;
			   st = con.prepareStatement(query);
			   st.setString(1, nombre);
			   st.setString(2, apellido);
			   st.setString(3, telefono);
			   st.setString(4, direccion);
			   st.setString(5, password);
               st.executeUpdate();

//			   st.executeUpdate(query);
	           
	           response.sendRedirect("Login.jsp");
	         
	           con.close();
	      }

	      catch(Exception e){
	   	  e.printStackTrace();

	      }
	   
	  }


}
