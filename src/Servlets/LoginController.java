package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import clases.Conexion;

@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})

public class LoginController extends HttpServlet {
 	   Connection con;
	   Statement st;
	   ResultSet rs;
	   HttpSession session;

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	
	try{
       }catch(Exception e){
	         
	        }
	    }

	   @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
	response.setContentType("text/html;charset=UTF-8");
	session = request.getSession();
	
	
      try{
          boolean bdatos = false;
    	  String usuario = request.getParameter("txtNombre");
          String password = request.getParameter("txtPassword");
          con = Conexion.getConnection();
          String consulta = "Select * from usuario where nombre=? and pass=?";
          ResultSet rs = null;
          PreparedStatement st = null;
          st = con.prepareStatement(consulta);
          st.setString(1, usuario);
          st.setString(2, password);
          rs = st.executeQuery();	           
          String elUsuarioEs="usuario";
          int tipoUser= 0;
          int ident  = 0;
          while(rs.next()){
            bdatos = true;
            tipoUser = Integer.parseInt(rs.getString("tipousuario"));
            ident    = Integer.parseInt(rs.getString("id"));
            if (tipoUser == 1) { 
                session.setAttribute("user", usuario);
           	    session.setAttribute("tipousuario", tipoUser);
           	    session.setAttribute("identificador", ident);
           	    request.getRequestDispatcher("/Home.jsp").forward(request, response);
              }
             else {
            	 session.setAttribute("user", usuario);
            	 session.setAttribute("identificador", ident);
            	 request.getRequestDispatcher("/HomeCliente.jsp").forward(request, response);

             }
              }
              if  (bdatos == false) {
            	  request.getRequestDispatcher("/Error.jsp").forward(request, response);  
            	  bdatos = true;
              }
          con.close();
      }
    catch(Exception e){
   	  e.printStackTrace();
    }
  }
}
