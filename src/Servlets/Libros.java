package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.Conexion;

/**
 * Servlet implementation class Libros
 */
@WebServlet("/Libros")
public class Libros extends HttpServlet {
  Connection con;
  Statement st;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Libros() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		response.setContentType("text/html;charset=UTF-8");
		
	      try{
	      	       String id = request.getParameter("txtid");
		           String titulo = request.getParameter("txttitulo");
		           String autor = request.getParameter("txtautor");
		           String reservado = request.getParameter("txtreservado");
		           String  estado = request.getParameter("txtestado");
		           String query = "";
		           con=Conexion.getConnection();
	               st=con.createStatement();
	               
	               query = "INSERT INTO libro( " + "id,  " + "titulo, " + "autor, " + "reservado, "
	                        + "estado" + ")VALUES (?,?,? ,?,?)";
	               PreparedStatement st = null;
					st = con.prepareStatement(query);
					st.setString(1, id);
					st.setString(2, titulo);
					st.setString(3, autor);
					st.setString(4, reservado);
					st.setString(5, estado);
					
					int dbalta = st.executeUpdate();
	               String sql=query;

	               st.executeUpdate(sql);
	   	    	  response.sendRedirect("Home.jsp" );


	             con.close();
	       }
	    catch(Exception e){
	   	  e.printStackTrace();

	    }
	   
	  }
}
