package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.Conexion;

/**
 * Servlet implementation class Reserva
 */
@WebServlet("/Reserva")
public class Reserva extends HttpServlet {
	   Connection con;
	   Statement st;
	   ResultSet rs;
	   
    public Reserva() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      try{
	  	   		   
		       String est;
 	    	   est = request.getParameter("txteditnom");
	  	   	   String fecini    = request.getParameter("txtfecini");
	  	   	   String fecfin    = request.getParameter("txtfecfin");
	  	   	   String codlib    = request.getParameter("txtid");
	  	   	   String codcli    = request.getParameter("txtcli");
	  	   	   String ident	    = request.getParameter("identificador");
	           String query        = "";
	           String query2	   = ""; 
	           int status=0;
              con = Conexion.getConnection();
              st=con.createStatement();
              query2=  "INSERT INTO reserva( " + "fecini,  " + "fecfin,  " + "codlib," + "cliente," + "identificador) VALUES (?,?,?,?,?)";
              query = "UPDATE libro SET estado= ' 0 ' , reservado = 'si' where libro.id='"+codlib+"'";
              PreparedStatement st = null;

				st = con.prepareStatement(query2);
				st.setString(1, fecini);
				st.setString(2, fecfin);
				st.setString(3, codlib);
				st.setString(4,codcli);
				st.setString(5,ident);
                st.executeUpdate();
                st.executeUpdate(query);
          
   	    	/*String sql = "UPDATE persona SET nombre='" + nom + "' where persona.identificador='"+id+"' " ;
   	    	st.executeUpdate(sql); */
   	    	
            response.sendRedirect("HomeCliente.jsp" );
   	    	
   	    	con.close();
      }
   catch(Exception e){
  	  e.printStackTrace();

   }	
	      }

}
