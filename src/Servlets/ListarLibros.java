package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import clases.Conexion;
import clases.Libro;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * Servlet implementation class ListarLibros
 */
@WebServlet("/ListarLibros")
public class ListarLibros extends HttpServlet {
	Connection con=null;	

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarLibros() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		PrintWriter out = response.getWriter();		
		HttpSession session = request.getSession(false);
		
		try {
		String tit = request.getParameter("txttitulo");

		PreparedStatement st = null;
		ResultSet rs = null;

		int activos = 1;
		List<Libro> listarLibro = new ArrayList<Libro>();
		String sql  = "select * from libro where titulo LIKE "+ "'%"+tit+"%'" ;		
		con=Conexion.getConnection();
		st = con.prepareStatement(sql);		
		ResultSet resulSet = st.executeQuery(sql);
 
		while (resulSet.next()) {
			int id = resulSet.getInt("id");
			String titulo = resulSet.getString("titulo");
			String autor = resulSet.getString("autor");
			String reservado = resulSet.getString("reservado");
			 int estado = resulSet.getInt("estado");
			
			Libro libro = new Libro(id, titulo, autor, 	reservado, estado);
			listarLibro.add(libro);
		}
		
			RequestDispatcher dispatcher = request.getRequestDispatcher("Video/Home.jsp");
			dispatcher.forward(request, response);
			request.setAttribute("lista", listarLibro);
			dispatcher.forward(request, response);
			
		con.close();
	} catch (SQLException ex) {
	       out.println(ex.toString());
       } catch (ClassNotFoundException e) {
		e.printStackTrace();
	}

	
	
	}


}


