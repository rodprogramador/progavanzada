package clases;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexion
{
	

   public static Connection getConnection()  throws SQLException, ClassNotFoundException { 
	   Connection con = null;

	   try {
		   String connectionURL = "jdbc:mysql://localhost:3306/bdlibro";
		   Class.forName("com.mysql.jdbc.Driver").newInstance();      
		   con = DriverManager.getConnection(connectionURL,"root", "");
	    } catch (Exception e) {
	        System.out.println(e);
	    }
	    return con;
	}
  }


