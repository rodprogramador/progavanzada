package clases;

public class Usuario {
	
	int clID;
	String clNombre;
	String clApellido;
	String clPassword;
	String clDireccion;
	int clTelefono;
	
	
	public Usuario(){
		super();
	}


	public int getClID() {
		return clID;
	}


	public void setClID(int clID) {
		this.clID = clID;
	}


	public String getClNombre() {
		return clNombre;
	}


	public void setClNombre(String clNombre) {
		this.clNombre = clNombre;
	}


	public String getClApellido() {
		return clApellido;
	}


	public void setClApellido(String clApellido) {
		this.clApellido = clApellido;
	}


	public String getClPassword() {
		return clPassword;
	}


	public void setClPassword(String clPassword) {
		this.clPassword = clPassword;
	}


	public String getClDireccion() {
		return clDireccion;
	}


	public void setClDireccion(String clDireccion) {
		this.clDireccion = clDireccion;
	}


	public int getClTelefono() {
		return clTelefono;
	}


	public void setClTelefono(int clTelefono) {
		this.clTelefono = clTelefono;
	}
	

	
	
}
