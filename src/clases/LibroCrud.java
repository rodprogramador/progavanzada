package clases;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import clases.Conexion;
import clases.Libro;

public class LibroCrud {

	public static List<Libro> getLibro()	
	   {
		  Connection con;
	      List<Libro> listaLibro= new ArrayList<Libro>();
	      try
	      {
	    	 con=Conexion.getConnection();
	         Statement st = con.createStatement();
	         ResultSet rs = st.executeQuery("select * from libro  " );
	         while (rs.next())
	         {
	            Libro libro = new Libro();
	            libro.setId(rs.getInt("id"));
	            libro.setTitulo(rs.getString("titulo"));
	            libro.setAutor(rs.getString("autor"));
	            libro.setReservado(rs.getString("reservado"));
	            listaLibro.add(libro);
	         }
	         
	         rs.close();
	         st.close();
	         con.close();
	      }
	      catch (Exception e)
	      {
	         e.printStackTrace();
	      }
	      return listaLibro;
	   }
	
	
}
