<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
    

<html>
<head>
<meta charset="ISO-8859-1">
<title>Libreria </title>
</head>
<body >
    <br>
    <div class="container-fluid">
        <div class="panel panel-success">
            <div class="panel-heading" align="center">
                <h4><b><font color="black" style="font-family: fantasy;">Libreria Carrera</font> </b></h4>
            </div>
            <div class="panel-body"align="center">
                 
                <div class="container " style="margin-top: 10%; margin-bottom: 10%;">
   
                    <div class="panel panel-success" style="max-width: 35%;" align="left">
                       
                        <div class="panel-heading form-group">
                            <b><font color="white">
                              Ingrese sus Credenciales </font> </b>
                        </div>
                   
                        <div class="panel-body" >

                        <form method="post" >
                            <div class="form-group">
                                <label for="exampleInputEmail1">Usuario</label> <input
                                    type="text" class="form-control" name="txtNombre" id="txtNombre"
                                    placeholder="Usuario" required="required">
                                   
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseņa</label> <input
                                    type="password" class="form-control" name="txtPassword" id="txtPassword"
                                    placeholder="Contraseņa" required="required">
                            </div>
                            <button type="submit" style="width: 100%; font-size:1.1em;" class="btn btn-large btn btn-success btn-lg btn-block" onclick="form.action='LoginController';" ><b>Ingresar</b></button>
                                                  
                        </form>

                        </div>
                    </div>
                   
                </div>
               
            </div>
            <div class="panel-footer" align="center"><font style="color: #000">Copyright @2019  <a href="https://www.linkedin.com/in/rodrigo-carrera/">LibreriaCarrera</a>,derechos reservados. </font></div>
        </div>
    </div>
 
</body>
</html>