<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import = "java.util.List"%> 
<%@ page import = "clases.Conexion"%>
<%@ page import = "java.sql.Statement"%>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.PreparedStatement"%>
<%@ page import = "java.sql.ResultSet"%>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Habilitar Usuario y Asignar Seguridad</title>
</head>
<body>

<h1 align="center">Seguridad Usuario.</h1>

<form method="post" action="HabUsuario">
	
     <table align="center">
       		<%
        	ResultSet rs = null;
			Connection con;
			String texto = request.getParameter("txttitulo");
			con=Conexion.getConnection();
	    	Statement st = con.createStatement();
		    String id = request.getParameter("id");
		    String sql  = "select * from usuario where usuario.id="+id;
		    rs = st.executeQuery(sql);
			while(rs.next()) {
			%> 
		    
		    <tr>
				<td class="titulotabla">ID Usuario:</td>
				<td><input type="number" class="form-control" readonly="" name="txtid" value="<%= rs.getInt("id") %>"></td>
			</tr>
		
    		<tr>
				<td class="titulotabla">Nombre * :</td>
				<td><input type="text"  class="form-control" name="txtnombre" value="<%= rs.getString("nombre") %>"></td>
			</tr>
	
			<tr>
				<td class="titulotabla">Apellido * :</td>
				<td><input type="text" class="form-control" name="txtapellido" value="<%= rs.getString("apellido") %>"></td>
			</tr>	
			
			<tr>
				<td class="titulotabla">Direccion * :</td>
				<td><input type="text"  class="form-control" name="txtdire" value="<%= rs.getString("direccion") %>" required></td>
			</tr>	
			
			<tr>
				<td class="titulotabla">Telefono * :</td>
				<td><input type="text"  class="form-control" name="txttel" value="<%= rs.getString("telefono") %>" required></td>
			</tr>
			
			
			<tr>
				<td class="titulotabla">Tipo Usuario * :</td>
				<td><input type="text"  class="form-control" name="txtTUsu" value="<%= rs.getString("tipousuario") %>"></td>
			</tr>			
	
			<td><input type="submit" name="btnEnviar" class="btn btn-success" value="Habilitar"></td>
			
			<%
		    }
	    	%> 
	    	    
	</table>
</form>	

	<form method="post" align="center">
		<br>
		<input type="submit" name="btnVolver" class="btn btn-success" value="Ir Pagina Principal" 
			onclick="form.action='Home.jsp';" >
	</form>
	

</body>
</html>