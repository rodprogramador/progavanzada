<!-- Los import -->
<%@ page language="java" %>
<%@ page import = "clases.Libro"%> 
<%@ page import = "clases.LibroCrud"%> 
<%@ page import = "java.util.List"%> 
<%@ page import = "clases.Conexion"%>
<%@ page import = "java.sql.Statement"%>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.PreparedStatement"%>
<%@ page import = "java.sql.ResultSet"%>
 
  
  <html>
	<body>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
	
	
   <%
	session=request.getSession(false);
	String usuario = "";
	String tipoUsuario="";
	String ident   = "";
	response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
	response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
	response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
	response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.

	 	
	if (session.getAttribute("tipousuario") != null && session.getAttribute("user") != null) {
	   request.getRequestDispatcher("/Home.jsp").forward(request, response);
	}
	
	if(session.getAttribute("user") == null){
		//String badlog = "La sesi�n caduc�. Ingrese nuevamente.";
   	//	request.setAttribute("error", badlog);
   		request.getRequestDispatcher("/SeguError.jsp").forward(request, response);
		
	} 
	else  {
		ident=session.getAttribute("identificador").toString();
		usuario=session.getAttribute("user").toString();
	}
   %>
	
	<form method="post" align="left">
		<h2 align="center" style="display: inline;"><%=usuario%></h2>
		<input type="submit" name="btnVolver" value="Cerrar Sesi�n" class="btn btn-success"
			onclick="form.action='LogOut';" >
			<br></br>
	</form>
	
	<h1>Devoluciones</h1>

	<form method="post">
	  <table>
	    <tr>
		  <td><input type="submit" class="btn btn-success" value="Pagina Principal"
		   	   onclick="form.action='HomeCliente.jsp';"></td>	   
		</tr>
	  </table>
	</form>

	<%
	     ResultSet rs = null;

		Connection con;
		String texto = request.getParameter("txttitulo");
		con=Conexion.getConnection();
	    Statement st = con.createStatement();
		String sql  = "select * from reserva , libro where reserva.codlib = libro.id and reserva.identificador =  '"+ident+"'  " ;
		rs = st.executeQuery(sql);

	%>	

	<table class="table table-bordered">
      <tr class="thead-dark">
		<th class="text-center">Id</th>
	   	<th class="text-center">Titulo</th>
	 	<th class="text-center">cliente</th>
		<th class="text-center">Reservado</th>
	  	<th class="text-center">Accion</th>
	  	
	  </tr>
		 <% 
		 while(rs.next()) {
		  
		 %> 
				
		 <tr>
		 	<td class="text-center"><%= rs.getInt("reserva.id") %> </td>
		    <td class="text-center"><%= rs.getString("libro.titulo") %></td> 
		    <td class="text-center"><%= rs.getString("reserva.cliente") %></td> 
		    <td class="text-center"><%= rs.getString("libro.reservado") %></td> 
		    
			<td class="text-center"><% if (rs.getInt("libro.estado") == 0 ) { %> <a class="btn btn-warning btn-sm" href="Devolucion.jsp?id=<%=rs.getInt("libro.id") %>">Devolucion</a><% } %></td> 
		    
		    
		  
			
					 
		 </tr>
		<%  } %>
    </table>
  
   </body>
  </html>