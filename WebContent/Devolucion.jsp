<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page language="java" %>
<%@ page import = "clases.Libro"%> 
<%@ page import = "java.util.List"%> 
<%@ page import = "clases.Conexion"%>
<%@ page import = "java.sql.Statement"%>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.PreparedStatement"%>
<%@ page import = "java.sql.ResultSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Devolucion</title>
</head>
<body>


   <%
	session=request.getSession(false);
	String usuario = "";
	String ident   = "";
	response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
	response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
	response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
	response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	if(session.getAttribute("user") == null){
		//String badlog = "La sesi�n caduc�. Ingrese nuevamente.";
   	//	request.setAttribute("error", badlog);
   		request.getRequestDispatcher("/Error.jsp").forward(request, response);
		
	} else {
		usuario=session.getAttribute("user").toString();
		ident=session.getAttribute("identificador").toString();
	}
   %>
	
	<form method="post" action="Devolucion">
	  <div id="dialogoverlay"></div>
	  <div id="dialogbox">
 	    <div>
 	      <div id="dialogboxhead"></div>
          <div id="dialogboxbody"></div>
  	      <div id="dialogboxfoot"></div>
        </div>
	  </div>
	  <fieldset align="center">
		<legend class="titulotabla">Ingrese sus datos</legend>
		<table align="center">
       		<%
        	ResultSet rs = null;
			Connection con;
			String texto = request.getParameter("txttitulo");
			con=Conexion.getConnection();
	    	Statement st = con.createStatement();
		    String id = request.getParameter("id");
		    String sql  = "select * from libro where libro.id="+id;
		    rs = st.executeQuery(sql);
			while(rs.next()) {
			%> 
			
		    <tr>
				<td class="titulotabla">ID Libro:</td>
				<td><input type="number" class= "form-control" readonly="" name="txtid" value="<%= rs.getInt("id") %>"></td>
			</tr>
		
    		<tr>
				<td class="titulotabla">Titulo * :</td>
				<td><input type="text" class= "form-control" readonly="" name="txttitulo" value="<%= rs.getString("titulo") %>"></td>
			</tr>
	
			<tr>
				<td class="titulotabla">Autor * :</td>
				<td><input type="text" class= "form-control" readonly="" name="txtautor" value="<%= rs.getString("autor") %>"></td>
			</tr>	
			
			  <tr>
				<td class="titulotabla">ID Cliente:</td>
				<td><input type="number" class= "form-control" readonly="" name="identificador" value="<%= ident  %>"></td>
			</tr>
			
			<tr>
				<td class="titulotabla">Cliente * :</td>
				<td><input type="text" class= "form-control" readonly="" name="txtcli" value="<%= usuario  %>"></td>
			</tr>			
	
			<td><input type="submit" name="btnEnviar" class="boton" value="Aceptar"></td>
			<%
		    }
	    	%> 
	    	    
		</table>
	  </fieldset>
	</form>
	<form method="post" align="center">
	  <br>
		<input type="submit" name="btnVolver" class="boton" value="Ir Pagina Principal" 
			onclick="form.action='HomeCliente.jsp';" >
	</form>
</body>
</html>