<!-- Los import -->
<%@ page language="java" %>
<%@ page import = "clases.Libro"%> 
<%@ page import = "clases.LibroCrud"%> 
<%@ page import = "java.util.List"%> 
<%@ page import = "clases.Conexion"%>
<%@ page import = "java.sql.Statement"%>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.PreparedStatement"%>
<%@ page import = "java.sql.ResultSet"%>
 

  <html>
	<body>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
	
   <%
	session=request.getSession(false);
	String usuario = "";
	response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
	response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
	response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
	response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	if(session.getAttribute("user") == null){
		//String badlog = "La sesi�n caduc�. Ingrese nuevamente.";
   	//	request.setAttribute("error", badlog);
   		request.getRequestDispatcher("/SeguError.jsp").forward(request, response);
		
	} else {
		usuario=session.getAttribute("user").toString();
	}
   %>
	
	<form method="post" align="">
		<h2 align="center" style="display: inline;"><%=usuario%></h2>
		<input type="submit" name="btnVolver" value="Cerrar Sesi�n" class="btn btn-success"
			onclick="form.action='LogOut';" >
			<br></br>
	</form>
	<h1>Libros</h1>

	<%
	     ResultSet rs = null;

		Connection con;
		String texto = request.getParameter("txttitulo");
		con=Conexion.getConnection();
	    Statement st = con.createStatement();
		if (texto != null) {
		String sql  = "select * from libro where titulo LIKE "+ "'%"+texto+"%'" ;
		rs = st.executeQuery(sql);
		}
		else {
		String sql  = "select * from libro " ;
		rs = st.executeQuery(sql);
		}
							
	%>	

	<form method="post">
		<table>
		  <tr>
			<td class="titulotabla">Titulo *</td>
			<td><input type="text" class="form-control" placeholder="Buscar" name="txttitulo" ></td>
		  	<td><input type="submit" class="btn btn-success" value="Buscar"></td>	
			<td><input type="submit" name="btnVolver" class="btn btn-info" value="Ingresar Libro" 
		   	   onclick="form.action='AltaLibro.jsp';" ></td>
		   	<td><input type="submit" class="btn btn-info" value="Ver Usuarios"
		   	   onclick="form.action='Usuarios.jsp';"></td>	   
		  </tr>
	    </table>  
	</form>

	<table class="table table-bordered">
      <tr class="thead-dark">
		<th class="text-center">Id</th>
	   	<th class="text-center">Titulo</th>
	 	<th class="text-center">Autor</th>
		<th class="text-center">Reservado</th>
	  	<th class="text-center">Accion</th>
	  	
	  </tr>
		 <% 
		 while(rs.next()) {
		  
		 %> 
				
		 <tr>
		 	<td class="text-center"><%= rs.getInt("id") %> </td>
		    <td class="text-center"><%= rs.getString("titulo") %></td> 
		    <td class="text-center"><%= rs.getString("autor") %></td> 
		    <td class="text-center"><%= rs.getString("reservado") %></td> 
			
			<td class="text-center"><% if (rs.getInt("estado") == 1 ) { %> <a class="btn btn-warning btn-sm" href="Reserva.jsp?id=<%=rs.getInt("id") %>">Reservar</a><% } %></td> 
					 
		 </tr>
		<%  } %>
    </table>
  
   </body>
  </html>