<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link type="text/css" href="plantillaBase.css" rel="stylesheet" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro</title>
</head>
<body>

<h1 align="center">REGISTRAR USUARIO</h1>
	<form method="post" action="Registro">
		<div id="dialogoverlay"></div>
		<div id="dialogbox">
 		 <div>
 		  <div id="dialogboxhead"></div>
  		  <div id="dialogboxbody"></div>
  		  <div id="dialogboxfoot"></div>
 		 </div>
		</div>
		
		<form method="post" align="center">
		<input type="submit" name="btnVolver" class="boton" value="Ir al Login" 
		   	   onclick="form.action='Login.jsp';" >
		</form><br>
		
		<fieldset align="center">
			<legend class="titulotabla">Ingrese sus datos</legend>
			<table>
				  <tr>
					<td class="titulotabla">Nombre *:</td>
					<td><input type="text" name="txtNombre" required ></td>
				</tr>
				<tr>
					<td class="titulotabla">Apellido *:</td>
					<td><input type="text" name="txtApellido" required></td>
				</tr>
	    	
	    		<tr>
					<td class="titulotabla">Contraseña *:</td>
					<td><input type="password" name="txtPass" required></td>
				</tr>
			
				<tr>
					<td class="titulotabla">Telefono *:</td>
					<td><input type="number" name="txtTel" required ></td>
				</tr>
				<tr>
				    <td class="titulotabla">Direccion *:</td>
				    <td><input type="text" name="txtDire" required></td>
				</tr> 
				<tr>
					<td colspan="2"><label>* Campos obligatorios</label> </td>
				
				</tr>
				
				
				<tr>
					<td><input type="submit" name="btnEnviar" class="boton" value="ACEPTAR"></td>
				</tr>
			</table>
		</fieldset>
	</form>

</body>
</html>