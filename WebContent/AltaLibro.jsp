<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
<h1 align="center">Alta Libro</h1>
	<form align="center" method="post" action="Libros">
		<div id="dialogoverlay"></div>
		<div id="dialogbox">
 		 <div>
 		  <div id="dialogboxhead"></div>
  		  <div id="dialogboxbody"></div>
  		  <div id="dialogboxfoot"></div>
 		 </div>
		</div>
		
			<legend class="titulotabla">Ingrese datos del Libro</legend></br>
			<table align="center">
				  <tr>
					<td class="titulotabla">Id *</td>
					<td><input type="number" class="form-control" name="txtid" required ></td>
				</tr>
				<tr>
					<td class="titulotabla">Titulo *</td>
					<td><input type="text" class="form-control" name="txttitulo" required></td>
				</tr>
	    	
	    		<tr>
					<td class="titulotabla">Autor*</td>
					<td><input type="text" class="form-control" name="txtautor" required></td>
				</tr>
			
				<tr>
					<td class="titulotabla">Reservado *</td>
					<td><input type="text" class="form-control" name="txtreservado" required ></td>
				</tr>
				<tr>
				    <td class="titulotabla">Estado *</td>
				    <td><input type="number" class="form-control" name="txtestado" required></td>
				</tr> 
				<tr>
					<td colspan="2"><label>* Campos obligatorios</label> </td>
				
				</tr>
				
				
				<tr>
					<td><input type="submit" name="btnEnviar" class="btn btn-success" value="ACEPTAR"></td>
					<td><input type="reset" class="btn btn-warning" value="LIMPIAR"></td>	
				</tr>
			</table>
		
	</form>
	<form method="post" align="center">
		<br>
		<input type="submit" name="btnVolver" class="btn btn-success" value="Ir Pagina Principal" 
			onclick="form.action='Home.jsp';" >
	</form>

</body>
</html>