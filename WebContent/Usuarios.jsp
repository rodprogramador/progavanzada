<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import = "java.util.List"%> 
<%@ page import = "clases.Conexion"%>
<%@ page import = "java.sql.Statement"%>
<%@ page import = "java.sql.Connection"%>
<%@ page import = "java.sql.PreparedStatement"%>
<%@ page import = "java.sql.ResultSet"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Usuarios</title>
</head>
<body>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>  
<h1 align="center">Usuarios</h1> <br>
	
	<%
	     ResultSet rs = null;

		Connection con;
		String texto = request.getParameter("txttitulo");
		con=Conexion.getConnection();
	    Statement st = con.createStatement();
		String sql  = "select * from usuario" ;
		rs = st.executeQuery(sql);
							
    %>	
	
	<table class="table table-bordered">
      <tr class="thead-dark">
		<th class="text-center">Id</th>
	   	<th class="text-center">Nombre</th>
	 	<th class="text-center">Apellido</th>
		<th class="text-center">Direccion</th>
	  	<th class="text-center">Telefono</th>
	  	<th class="text-center">Tipo Usuario</th>
	  	
	  </tr>
		 <% 
		 while(rs.next()) {
		  
		 %> 
				
		 <tr>
		 	<td class="text-center" ><%= rs.getInt("id") %> </td>
		    <td class="text-center"><%= rs.getString("nombre") %></td> 
		    <td class="text-center"><%= rs.getString("apellido") %></td> 
		    <td class="text-center"><%= rs.getString("direccion") %></td> 
			<td class="text-center"><%= rs.getString("telefono") %></td>
			<td class="text-center"><% if (rs.getInt("tipousuario") == 0 ) { %> <a class="btn btn-warning btn-sm"  href="HabUsuario.jsp?id=<%=rs.getInt("id") %>">Cliente-Admin</a><% } %></td> 
					 
		 </tr>
		<%  } %>
    </table>
	<form method="post" align="center">
		<br>
		<input type="submit" name="btnVolver" class="btn btn-success" value="Ir Pagina Principal" 
			onclick="form.action='Home.jsp';" >
	</form>

</body>
</body>
</html>